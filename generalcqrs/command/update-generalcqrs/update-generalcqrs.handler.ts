import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateGeneralCqrsCommand } from './update-generalcqrs.command';
import { GeneralCqrsAggregateService } from '../../aggregates/generalcqrs-aggregate/generalcqrs-aggregate.service';

@CommandHandler(UpdateGeneralCqrsCommand)
export class UpdateGeneralCqrsCommandHandler
  implements ICommandHandler<UpdateGeneralCqrsCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: GeneralCqrsAggregateService,
  ) { }

  async execute(command: UpdateGeneralCqrsCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
