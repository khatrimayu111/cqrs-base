import { Test, TestingModule } from '@nestjs/testing';
import { GeneralCqrsWebhookController } from './generalcqrs-webhook.controller';
import { GeneralCqrsWebhookAggregateService } from '../../../generalcqrs/aggregates/generalcqrs-webhook-aggregate/generalcqrs-webhook-aggregate.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';

describe('GeneralCqrsWebhook Controller', () => {
  let controller: GeneralCqrsWebhookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: GeneralCqrsWebhookAggregateService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
      ],
      controllers: [GeneralCqrsWebhookController],
    }).compile();

    controller = module.get<GeneralCqrsWebhookController>(GeneralCqrsWebhookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
