import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GeneralCqrsService } from './generalcqrs.service';
import { GeneralCqrs } from './generalcqrs.entity';

describe('generalCqrsService', () => {
  let service: GeneralCqrsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GeneralCqrsService,
        {
          provide: getRepositoryToken(GeneralCqrs),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<GeneralCqrsService>(GeneralCqrsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
